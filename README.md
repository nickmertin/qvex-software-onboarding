# QVEX Software Onboarding

This is a modified version of the [rustlings](https://github.com/rust-lang/rustlings) course by Queen's VEXU Robotics for software team onboarding.

## Video Tutorial

https://youtu.be/rTuIunr_wWs
